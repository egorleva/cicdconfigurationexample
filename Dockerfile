FROM eclipse-temurin:17.0.8.1_1-jdk

ENV ANDROID_TARGET_SDK="34"
ENV ANDROID_BUILD_TOOLS="34.0.0"
ENV ANDROID_SDK_TOOLS="11076708_latest"
ENV ANDROID_HOME $PWD/android-sdk
ENV GRADLE_USER_HOME=$PWD/gradle

USER root

RUN apt-get update

RUN apt-get -y --no-install-recommends --allow-unauthenticated install \
    curl \
    unzip

RUN mkdir -p ${ANDROID_HOME}/cmdline-tools/latest

RUN cd ${ANDROID_HOME}/cmdline-tools/latest \
    && curl -o cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}.zip \
    && unzip -q cmdline-tools.zip \
    && rm cmdline-tools.zip

RUN mv ${ANDROID_HOME}/cmdline-tools/latest/cmdline-tools/* ${ANDROID_HOME}/cmdline-tools/latest \
    && rmdir ${ANDROID_HOME}/cmdline-tools/latest/cmdline-tools

RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager "platforms;android-${ANDROID_TARGET_SDK}" >/dev/null

RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager "platform-tools" >/dev/null

RUN echo y | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null

RUN apt-get clean \
    && apt-get autoremove \
    && rm -Rf /tmp/* /var/tmp/* /var/lib/apt/lists/*